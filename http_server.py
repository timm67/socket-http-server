import socket
import os
import sys
import traceback

HTTP_VER=b"HTTP/1.1 "
RESP_OK=b"200 OK"
RESP_NOT_ALWD=b"405 Operation not permitted"
RESP_NOT_FOUND=b"404 Not Found"
HTTP_CRLF=b"\r\n"

def response_ok(body=b"This is a minimal response", mimetype=b"text/plain"):
    resp = b""
    """
    returns a basic HTTP response
    Ex:
        response_ok(
            b"<html><h1>Welcome:</h1></html>",
            b"text/html"
        ) ->

        b'''
        HTTP/1.1 200 OK\r\n
        Content-Type: text/html\r\n
        \r\n
        <html><h1>Welcome:</h1></html>\r\n
        '''
    """
    if mimetype == b"text/html":
        resp = HTTP_VER + RESP_OK + HTTP_CRLF + \
        b"Content-Type: " + mimetype + HTTP_CRLF + HTTP_CRLF + \
        b"<html><h1>" + body + b"</h1></html>" + HTTP_CRLF
    elif mimetype == b"text/plain":
        resp = HTTP_VER + RESP_OK + HTTP_CRLF + \
        b"Content-Type: " + mimetype + HTTP_CRLF + HTTP_CRLF + body
    elif mimetype == b"image/png":
        resp = HTTP_VER + RESP_OK + HTTP_CRLF + \
        b"Content-Type: " + mimetype + HTTP_CRLF + HTTP_CRLF + body
    elif mimetype == b"image/jpeg":
        resp = HTTP_VER + RESP_OK + HTTP_CRLF + \
        b"Content-Type: " + mimetype + HTTP_CRLF + HTTP_CRLF + body

    return resp

def response_method_not_allowed():
    """Returns a 405 Method Not Allowed response"""
    resp = HTTP_VER + RESP_NOT_ALWD + HTTP_CRLF + HTTP_CRLF
    return resp


def response_not_found():
    """Returns a 404 Not Found response"""
    resp = HTTP_VER + RESP_NOT_FOUND + HTTP_CRLF + HTTP_CRLF
    return resp


def parse_request(request):
    """
    Given the content of an HTTP request, returns the path of that request.
    """

    """
    This server only handles GET requests, so this method shall raise a
    NotImplementedError if the method of the request is not GET.
    """
    if "GET" not in request:
        raise NotImplementedError

    tokens = request.split(" ")
    if tokens[0] == "GET":
        return tokens[1]
    else:
        return ""

def response_path(path):
    """
    This method should return appropriate content and a mime type.

    If the requested path is a directory, then the content should be a
    plain-text listing of the contents with mimetype `text/plain`.

    If the path is a file, it should return the contents of that file
    and its correct mimetype.

    If the path does not map to a real location, it should raise an
    exception that the server can catch to return a 404 response.

    Ex:
        response_path('/a_web_page.html') -> (b"<html><h1>North Carolina...",
                                            b"text/html")

        response_path('/images/sample_1.png')
                        -> (b"A12BCF...",  # contents of sample_1.png
                            b"image/png")

        response_path('/') -> (b"images/, a_web_page.html, make_type.py,...",
                             b"text/plain")

        response_path('/a_page_that_doesnt_exist.html') -> Raises a NameError

    """
    # Fill in the appropriate content and mime_type give the path.
    # See the assignment guidelines for help on "mapping mime-types", though
    # you might need to create a special case for handling make_time.py
    #
    # If the path is "make_time.py", then you may OPTIONALLY return the
    # result of executing `make_time.py`. But you need only return the
    # CONTENTS of `make_time.py`.
    
    content = b"not implemented"
    mime_type = b"not implemented"

    initial_path = os.getcwd()
    initial_path += os.sep + 'webroot'
    full_path = initial_path + path

    # Raise a NameError if the requested content is not present
    # under webroot.
    if os.path.exists(full_path) is not True:
        raise NameError

    if os.path.isdir(full_path) is True:
        mime_type = b"text/plain"
        file_list = os.listdir(full_path)
        # list comprehension !!!
        file_list = [filename.encode('utf-8') for filename in file_list]
        content = b'\n'.join(file_list)
    elif full_path.endswith('.png'):
        mime_type = b"image/png"
        with open(full_path, 'rb') as f:
            content = f.read()
    elif full_path.endswith('.jpg'):
        mime_type = b"image/jpeg"
        with open(full_path, 'rb') as f:
            content = f.read()
    elif full_path.endswith('.html'):
        mime_type = b"text/html"
        with open(full_path, 'r') as f:
            content = f.read()
        content = content.encode('utf-8')
    elif full_path.endswith('.txt'):
        mime_type = b"text/plain"
        with open(full_path, 'r') as f:
            content = f.read()
        content = content.encode('utf-8')
    elif full_path.endswith('.py'):
        mime_type = b"text/plain"
        with open(full_path, 'r') as f:
            content = f.read()
        content = content.encode('utf-8')

    return content, mime_type


def server(log_buffer=sys.stderr):
    address = ('127.0.0.1', 10000)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print("making a server on {0}:{1}".format(*address), file=log_buffer)
    sock.bind(address)
    sock.listen(1)

    try:
        while True:
            print('waiting for a connection', file=log_buffer)
            conn, addr = sock.accept()  # blocking
            try:
                print('connection - {0}:{1}'.format(*addr), file=log_buffer)

                request = ''
                while True:
                    data = conn.recv(1024)
                    request += data.decode('utf8')

                    if '\r\n\r\n' in request:
                        break
		

                print("Request received:\n{}\n\n".format(request))

                # TODO: Use parse_request to retrieve the path from the request.
                try:
                    path = parse_request(request)

                    # Use response_path to retrieve the content and the mimetype,
                    # based on the request path.

                    content, mime_type = response_path(path)
                    response = response_ok(content, mime_type)

                    # If parse_request raised a NotImplementedError, then let
                    # response be a method_not_allowed response. If response_path raised
                    # a NameError, then let response be a not_found response. Else,
                    # use the content and mimetype from response_path to build a 
                    # response_ok.

                except NotImplementedError:
                    response = response_method_not_allowed()
                except NameError:
                    response = response_not_found()

                conn.sendall(response)
            except:
                traceback.print_exc()
            finally:
                conn.close() 

    except KeyboardInterrupt:
        sock.close()
        return
    except:
        traceback.print_exc()


if __name__ == '__main__':
    server()
    sys.exit(0)


